// Cau 1:
function xuatSoNguyen(){
    event.preventDefault();
    var number1 = document.getElementById("txt-so-nguyen-1").value*1;
    var number2 = document.getElementById("txt-so-nguyen-2").value*1;
    var number3 = document.getElementById("txt-so-nguyen-3").value*1;
    var number = [number1,number2,number3];
    number.sort(function(a,b){return a-b});
   document.getElementById("result-1").innerHTML =`Thứ tự số nguyên tăng dần là: ${number}`;
}   
// Cau 2:
const HELLO_FT ="hello-ft"
const HELLO_MT ="hello-mt"
const HELLO_BR ="hello-br"
const HELLO_ST ="hello-st"

function chaoHoiGiaDinh(){
    event.preventDefault();
  var e=document.getElementById("helloUser").value;
  if(e==HELLO_FT){
      document.getElementById("result-2").innerHTML=`Xin chào bố!`;
  }else if(e==HELLO_MT){
    document.getElementById("result-2").innerHTML=`Xin chào mẹ!`;
  }else if(e==HELLO_BR){
    document.getElementById("result-2").innerHTML=`Xin chào anh trai!`;
  }else if (e==HELLO_ST){
    document.getElementById("result-2").innerHTML=`Xin chào em gái!`;
  }else{
    document.getElementById("result-2").innerHTML=`Xin chào người lạ!`;
  }
}
// Cau 3:
function chiaSoNguyen(){
    event.preventDefault();
    let dem = 0;
    var e = document.getElementById("txt-so-1").value*1;
    var t = document.getElementById("txt-so-2").value*1;
    var n = document.getElementById("txt-so-3").value*1;
    if(e%2==0){
      dem++;
      console.log("Số chẵn");
    }
    if(t%2==0){
      dem++;
      console.log("Số chẵn");
    }
    if(n%2==0){
      dem++;
      console.log("Số chẵn");
    }
    document.getElementById("result-3").innerHTML=`Có ` +dem+ ` số chẵn, ` +(3-dem)+ ` số lẻ`;
}

// Cau 4:
function timTamGiac(){
  event.preventDefault();
  var a = document.getElementById("txt-canh-1").value*1;
  var b = document.getElementById("txt-canh-2").value*1;
  var c = document.getElementById("txt-canh-3").value*1;
  if(a==b && b==c){
    document.getElementById("result-4").innerHTML = `Là tam giác đều`
  }else if(a==b || a==c || b==c){
    document.getElementById("result-4").innerHTML = `Là tam giác cân`
  }else if(Math.sqrt(Math.pow(b,2)+Math.pow(c,2))==a || Math.sqrt(Math.pow(a,2)+Math.pow(c,2))==b || Math.sqrt(Math.pow(c,2)+Math.pow(b,2))==c){
    document.getElementById("result-4").innerHTML = `Là tam giác vuông`
  }else{
    document.getElementById("result-4").innerHTML = `Là tam giác gì đó`
  }
}
// Cau 5:  
// 31 ngay: 1,3,5,7,8,10,12
// 30 ngay: 4,6,9,11
function ngayHomTruoc(){
  event.preventDefault();
  var a = Number(document.getElementById("txt-ngay").value);
  var b = Number(document.getElementById("txt-thang").value) ;
  var c = Number(document.getElementById("txt-nam").value) ;
  var t = "";
  if(a>1 && a<=31 && b<=12){
    t =`Ngày trước đó là: `+ (a-1) + `/` +b +`/`+c;
  }if(a==1 && (b==5||b==7||b==10||b==12)){
    t = `Ngày trước đó là: `+ (a+29) + `/` +(b-1) +`/`+c;
  }if(a==1 && b==3){
    t = `Ngày trước đó là: `+ (a+27) + `/` +(b-1) +`/`+c;
  }if(a==1 && (b==2||b==4||b==6||b==8||b==9||b==11)){
    t = `Ngày trước đó là: `+ (a+30) + `/` +(b-1) +`/`+c;
  }if(a==1 && b==1){
    t = `Ngày trước đó là: `+ (a+30) + `/` +(b+11) +`/`+(c-1);
  }if(b==2 && a>28){
    t = `Ngày không xác định`
  }if(a<0 || a>31){
    t = `Ngày không xác định`
  }if(b<1 || b>12){
    t = `Tháng không xác định`
  }if(c<=0){
    t = `Năm không xác định`
  }
  document.getElementById("result-5").innerHTML = t;
}
function ngayHomSau(){
  event.preventDefault();
  var a = Number(document.getElementById("txt-ngay").value);
  var b = Number(document.getElementById("txt-thang").value) ;
  var c = Number(document.getElementById("txt-nam").value) ;
  var t = "";
  if(a>=1 && b<=12){
    t =`Ngày hôm sau là: `+ (a+1) + `/` +b +`/`+c;
  }if(a==31 && (b==1||b==3||b==5||b==7||b==8||b==10)){
    t = `Ngày hôm sau là: `+ (a-30) + `/` +(b+1) +`/`+c;
  }
  if(a==28 && b==2){
    t = `Ngày hôm sau là: `+ (a-27) + `/` +(b+1) +`/`+c;
  }if(a==30 && (b==4||b==6||b==9||b==11)){
    t = `Ngày hôm sau là: `+ (a-29) + `/` +(b+1) +`/`+c;
  }if(a==31 && b==12){
    t = `Ngày hôm sau là: `+ (a-30) + `/` +(b-11) +`/`+(c+1);
  }if(b==2 && a>28){
    t = `Ngày không xác định`
  }if(a<0 || a>31){
    t= `Ngày không xác định`
  }if(b<1 || b>12){
    t = `Tháng không xác định`
  }if(c<=0){
    t = `Năm không xác định`
  }
  document.getElementById("result-5").innerHTML = t;
}
// Cau 6:
// 31 ngay: 1,3,5,7,8,10,12
// 30 ngay: 4,6,9,11
function thangCoBaoNhieuNgay(){
  event.preventDefault();
  var a= Number(document.getElementById("txt-nhap-thang").value);
  var b= Number(document.getElementById("txt-nhap-nam").value);
  var t="";
  if(a==1||a==3||a==5||a==7||a==8||a==10||a==12){
    t=`Tháng `+ a+ ` năm ` +b+ ` có 31 ngày`;
  }else if(a==4||a==6||a==9||a==11){
    t=`Tháng `+ a+ ` năm ` +b+ ` có 30 ngày`;
  }else if(a==2 && ((b%4===0 && b%100!==0 && b%400!==0)||(b%400===0))){
    t=`Tháng `+ a+ ` năm ` +b+ ` có 29 ngày`;
  }else{
    t = `Tháng `+ a+ ` năm ` +b+ ` có 28 ngày`;
  }
  document.getElementById("result-6").innerHTML = t;
}
// Cau 7:
// Nếu 
function cachDocSo(){
  event.preventDefault();
  var e = document.getElementById("txt-doc-so").value*1;
  var hangTram = Math.floor(e/100);
  var hangChuc = Math.floor((e%100)/10);
  var hangDonVi = Math.floor((e%100)%10);
  var a = "";
  var b = "";
  var c = "";
  if(hangTram==1){
    a = `một trăm`;
  }else if(hangTram==2){
    a = ` hai trăm`;
  }else if(hangTram==3){
    a = ` ba trăm`;
  }else if(hangTram==4){
    a = ` bốn trăm`;
  }else if(hangTram==5){
    a = ` năm trăm`;
  }else if(hangTram==6){
    a = ` sáu trăm`;
  }else if(hangTram==7){
    a = ` bảy trăm`;
  }else if(hangTram==8){
    a = ` tám trăm`;
  }else if(hangTram==9){
    a = ` chín trăm`;
  }
  if(hangChuc==1 ){
    b = ` mười`;
  }else if(hangChuc==2){
    b = ` hai mươi`;
  }else if(hangChuc==3){
    b = ` ba mươi`;
  }else if(hangChuc==4){
    b = ` bốn mươi`;
  }else if(hangChuc==5){
    b = ` năm mươi`;
  }else if(hangChuc==6){
    b = ` sáu mươi`;
  }else if(hangChuc==7){
    b = ` bảy mươi`;
  }else if(hangChuc==8){
    b = ` tám mươi`;
  }else if(hangChuc==9){
    b = ` chín mươi`;
  }if(hangChuc == 0 && hangDonVi == 0 ){
    b = "";
  }else if(hangChuc == 0 && hangDonVi>0){
    b = " lẻ";
  }
  if(hangDonVi==0){
    c = ``;
  }else if(hangDonVi==1 ){
    c = `một `;
  }else if(hangDonVi==2){
    c = ` hai `;
  }else if(hangDonVi==3){
    c = ` ba `;
  }else if(hangDonVi==4){
    c = ` bốn `;
  }else if(hangDonVi==5){
    c = ` năm `;
  }else if(hangDonVi==6){
    c = ` sáu `;
  }else if(hangDonVi==7){
    c = ` bảy `;
  }else if(hangDonVi==8){
    c = ` tám `;
  }else if(hangDonVi==9){
    c = ` chín `;
  }
  if(e>=100&&e<=999){
    document.getElementById("result-7").innerHTML = a + b + c;
  }else{
    document.getElementById("result-7").innerHTML =alert( `Không xác định`);
  }
}
// Cau 8:
function timToaDo(){
  event.preventDefault();
  var ten1 =  document.getElementById("txt-nhap-ten1").value;
  var ten2 =  document.getElementById("txt-nhap-ten2").value;
  var ten3 =  document.getElementById("txt-nhap-ten3").value;
  var x1 = document.getElementById("txt-nhap-x1").value;
  var x2 = document.getElementById("txt-nhap-x2").value;
  var x3 = document.getElementById("txt-nhap-x3").value;
  var y1 = document.getElementById("txt-nhap-y1").value;
  var y2 = document.getElementById("txt-nhap-y2").value;
  var y3 = document.getElementById("txt-nhap-y3").value;
  var x4 = document.getElementById("txt-nhap-x4").value;
  var y4 = document.getElementById("txt-nhap-y4").value;
  var a = Math.sqrt(Math.pow(x4-x1,2)+Math.pow(y4-y1,2));
  var b = Math.sqrt(Math.pow(x4-x2,2)+Math.pow(y4-y2,2));
  var c = Math.sqrt(Math.pow(x4-x3,2)+Math.pow(y4-y3,2));
  var d = "";
  if (a>b && a>c){
    d = ten1; 
  }else if(b>a && b>c){
    d = ten2;
  }else{
    d = ten3;
  }
  document.getElementById("result-8").innerHTML = `Sinh viên xa trường nhất là `+ d;
}